package test;

import boundaryclasses.IHumiditySensor;

public class HumiditySensorStub implements IHumiditySensor {
	
	private volatile double value;
	private volatile boolean checkedHum;
	
	@Override
	public double getHumidity() {
		checkedHum = true;
	    return value;
	}
	
	public void setHumidity(double value)
	{
		this.value = value;
	}
	
	public boolean checkedHum()
	{
	    return checkedHum;
	}
	
	public void resetChecked()
	{
	    checkedHum = false;
	}

}