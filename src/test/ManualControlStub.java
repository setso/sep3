package test;

import boundaryclasses.IManualControl;

public class ManualControlStub implements IManualControl {

    private volatile boolean acknowledged;
	private volatile boolean receivedAcknowledged;
    
	@Override
	public boolean receivedAcknowledgement() {
		boolean result = receivedAcknowledged;
		receivedAcknowledged = false;
	    return result;
	}
	
	public void setAcknowledged(boolean acknowledged){
	    this.acknowledged = acknowledged;
		receivedAcknowledged = acknowledged;
	}

}
