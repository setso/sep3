package test;

import boundaryclasses.ITimer;

public class TimerStub implements ITimer {
	
	private volatile boolean timerExpired;

	@Override
	public void startTime(double seconds) {
		if(seconds > 0){
		    timerExpired = false;
		}
	}

	@Override
	public boolean isTimerExpired() {
		return timerExpired;
	}
	
	public void setExpired(){
	    timerExpired = true;
	}
	
	public boolean hasBeenStarted(){
	    return !timerExpired;
	}

}
