package test;

import boundaryclasses.IGate;

public class GateStub implements IGate {

	private volatile boolean gateOpen;
	private volatile boolean gateClosed;
	private volatile boolean isClosing;
	private volatile boolean isOpening;
	private volatile boolean receivedClosed;
	private volatile boolean receivedOpen;
	
	public void setGateState(boolean open){
	    gateOpen = open;
	    gateClosed = !open;
	    isClosing = false;
	    isOpening = false;
		receivedClosed = !open;
		receivedOpen = open;
	}
	
	public boolean isOpening(){
	    return isOpening;
	}
	
	public boolean isClosing(){
	    return isClosing;
	}

	@Override
	public void sendCloseGate() {
		isClosing = true;
	}

	@Override
	public void sendOpenGate() {
        isOpening = true;
	}

	@Override
	public boolean receivedGateClosed() {
		boolean result = receivedClosed;
		receivedClosed = false;
		return result;
	}

	@Override
	public boolean receivedGateOpen() {
		boolean result = receivedOpen;
		receivedOpen = false;
		return result;
	}

}
