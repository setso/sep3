package test;

import boundaryclasses.IOpticalSignals;

public class OpticalSignalsStub implements IOpticalSignals {
	
	private volatile boolean lampA;
	private volatile boolean lampB;

	@Override
	public void switchLampAOn() {
		lampA = true;
	}

	@Override
	public void switchLampAOff() {
        lampA = false;
	}

	@Override
	public void switchLampBOn() {
        lampB = true;
	}

	@Override
	public void switchLampBOff() {
        lampB = false;
	}
	
	public boolean aSwitchedOn()
	{
	    return lampA;
	}
	
	public boolean bSwitchedOn()
	{
	    return lampB;
	}

}
