package test;

import boundaryclasses.IPump;

public class PumpStub implements IPump {
	
	private volatile boolean pumpOn;
	private volatile boolean hasBeenActivated;
	private volatile boolean hasBeenDeactivated;
	private volatile boolean receivedActivated;
	
	@Override
	public void sendActivate() {
        hasBeenActivated = true;
	}

	@Override
	public void sendDeactivate() {
        hasBeenDeactivated = true;
	}

	@Override
	public boolean receivedActivated() {
		boolean result = receivedActivated;
		receivedActivated = false;
		return result;
	}
	
	public boolean hasBeenActivated(){
	    return hasBeenActivated;
	}
	
	public boolean hasBeenDeactivated(){
	    return hasBeenDeactivated;
	}
	
	public void setActive(boolean active){
	    pumpOn = active;
		receivedActivated = active;
	}
	
	public void reset(){
	    hasBeenActivated = false;
	    hasBeenDeactivated = false;
	}

}
