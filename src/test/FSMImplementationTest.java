package test;
/**
 * Test Framework for testing the FSM from Practice 3
 * @author Thomas Lehmann
 * @version 2015-11-18
 */
import static org.junit.Assert.*;
import fsm.IFSM;
import implementation.FSMImplementation;
import implementation.FSMState;

import org.junit.Before;
import org.junit.Test;

import boundaryclasses.IGate;
import boundaryclasses.IHumidifier;
import boundaryclasses.IHumiditySensor;
import boundaryclasses.IManualControl;
import boundaryclasses.IOpticalSignals;
import boundaryclasses.IPump;

public class FSMImplementationTest {
    
    public static final int DELAY = 500;
    
	private PumpStub pumpA;
	private PumpStub pumpB;
	private GateStub gate;
	private OpticalSignalsStub signals;
	private HumiditySensorStub sensor;
	private HumidifierStub humidifier;
	private ManualControlStub operatorPanel;
	private TimerStub timer;
	private IFSM uut;
	
	@Before
	public void testSetup(){
		pumpA = new PumpStub();
		pumpB = new PumpStub();
		gate = new GateStub();
		signals = new OpticalSignalsStub();
		sensor = new HumiditySensorStub();
		humidifier = new HumidifierStub();
		operatorPanel = new ManualControlStub();
		timer = new TimerStub();
		uut = new FSMImplementation(  pumpA,  pumpB,  gate,  signals,
				humidifier,  sensor,  operatorPanel, timer) ;
		
        timer.setExpired();
        pumpA.setActive(false);
        pumpB.setActive(false);
        pumpA.reset();
        pumpB.reset();
        gate.setGateState(false);
        sensor.setHumidity(25);
	}
	
	@Test
	public void testPath() {
	    
	    uut.evaluate();
	    
	    assertTrue(sensor.checkedHum());
	
		//Humidity exceeding normal range, expecting state change
		sensor.setHumidity(100);
		gate.setGateState(true); //true == open
		
		uut.evaluate();
		
		assertTrue(signals.bSwitchedOn());
		assertTrue(gate.isClosing());
		
        uut.evaluate();
		
		//Gate has not closed yet, expecting no state change
		assertTrue(signals.bSwitchedOn());
		
		//Closing gate
		gate.setGateState(false);
		
        uut.evaluate();
		
		//Gate is now closed, expecting state change
		assertFalse(signals.bSwitchedOn());
	    assertTrue(pumpA.hasBeenActivated());
	    assertTrue(pumpB.hasBeenActivated());
		assertTrue(timer.hasBeenStarted());
				
		//Activating pump A
		pumpA.setActive(true);
				
        uut.evaluate();
		
		
		//Pumps are expected to be activated so they need to be initialised
		pumpA.reset();
		pumpB.reset();
		
		//Timer expired, expecting state change
		timer.setExpired();
		
        uut.evaluate();
		
		assertTrue(pumpA.hasBeenDeactivated());
		assertTrue(pumpB.hasBeenDeactivated());
	    assertTrue(signals.bSwitchedOn());
	    assertTrue(gate.isOpening());
	    
        //Opening gate
        gate.setGateState(true);
	    
	    //Initialising manual reset
	    operatorPanel.setAcknowledged(false);
	    
        uut.evaluate();
	    
	    //Gate is now open, expecting state change
	    assertFalse(signals.bSwitchedOn());
	    
        uut.evaluate();
	    
	    //Set Humidity to normal
	    sensor.setHumidity(25);
	    sensor.resetChecked();
	    
	    //Accessing operator panel
	    operatorPanel.setAcknowledged(true);
	    
        uut.evaluate();
	    
	    assertTrue(sensor.checkedHum());
	}
}
