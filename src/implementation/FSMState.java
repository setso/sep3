package implementation;

public enum FSMState 
{
	CheckingHumidity, 
	ClosingGate, 
	OpeningGate, 
	Spraying, 
	WaitForPumps, 
	PumpsOn, 
	PumpAOn, 
	PumpBOn, 
	Error, 
	ErrOpeningGate
	
}
