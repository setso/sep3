package implementation;
import boundaryclasses.IGate;
import boundaryclasses.IHumidifier;
import boundaryclasses.IHumiditySensor;
import boundaryclasses.IManualControl;
import boundaryclasses.IOpticalSignals;
import boundaryclasses.IPump;
import boundaryclasses.ITimer;
import fsm.IFSM;


public class FSMImplementation implements IFSM {
	private FSMState state;
	private IPump pumpA;
	private IPump pumpB;
	private IGate gate;
	private ITimer timer;
	private IOpticalSignals signals;
	private IHumiditySensor sensor;
	private IHumidifier humidifier;
	private IManualControl operatorPanel;
	private final double upperBound;
	private final double lowerBound;
	private double humidity;

	public FSMImplementation( IPump pumpA, IPump pumpB, IGate gate, IOpticalSignals signals,
			IHumidifier humidifier, IHumiditySensor sensor, IManualControl operatorPanel, ITimer timer) {
		this.state = FSMState.CheckingHumidity;
		this.pumpA = pumpA;
		this.pumpB = pumpB;
		this.gate = gate;
		this.signals = signals;
		this.sensor = sensor;
		this.humidifier = humidifier;
		this.operatorPanel = operatorPanel;
		upperBound = 60;
		lowerBound = 20;
		this.timer = timer;
	}
	
	@Override
	public void evaluate() 
	{
		//while(true)
		//{
			humidity = sensor.getHumidity();
			switch(state)
			{
				case CheckingHumidity: 
					if(humidity < lowerBound)
					{
						signals.switchLampAOn();
						humidifier.sendSprayOn();
						state = FSMState.Spraying;
					}
					else if(humidity > upperBound)
					{
						gate.sendCloseGate();
						signals.switchLampBOn();
						state = FSMState.ClosingGate;
					}
					break;
				
				case ClosingGate: 
					
					if(gate.receivedGateClosed())
					{
						signals.switchLampBOff();
						pumpA.sendActivate();
						pumpB.sendActivate();
						timer.startTime(5);
						state = FSMState.WaitForPumps;
					}
					break;
				
				case Spraying: 
					
					if(humidity >= lowerBound)
					{
						signals.switchLampAOff();
						humidifier.sendSprayOff();
						state = FSMState.CheckingHumidity;
					}
					break;
				
				case Error: 
					if(operatorPanel.receivedAcknowledgement())
					{
						state = FSMState.CheckingHumidity;
					}
					break;
				
				case ErrOpeningGate:

					if(gate.receivedGateOpen())
					{
						signals.switchLampBOff();
						state = FSMState.Error;
					}
					break;
				
				case WaitForPumps: 
					
					if(pumpA.receivedActivated())
					{
						state = FSMState.PumpAOn;
					}
					else if(pumpB.receivedActivated())
					{
						state = FSMState.PumpBOn;
					}
					
					else if(timer.isTimerExpired())
					{
						signals.switchLampBOn();
						gate.sendOpenGate();
						pumpA.sendDeactivate();
						pumpB.sendDeactivate();
						state = FSMState.ErrOpeningGate;
					}
					
					break;
				
				case PumpAOn: 
					if(pumpB.receivedActivated())
					{
						state = FSMState.PumpsOn;
					}
					else if(timer.isTimerExpired())
					{
						signals.switchLampBOn();
						gate.sendOpenGate();
						state = FSMState.ErrOpeningGate;
						pumpA.sendDeactivate();
						pumpB.sendDeactivate();
					}
					
					break;
				
				case PumpBOn: 
					if(pumpA.receivedActivated())
					{
						state = FSMState.PumpsOn;
					}
					else if(timer.isTimerExpired())
					{
						signals.switchLampBOn();
						gate.sendOpenGate();
						state = FSMState.ErrOpeningGate;
	                    pumpA.sendDeactivate();
	                    pumpB.sendDeactivate();
					}
					break;
				
				case PumpsOn: 
					if(humidity <= upperBound)
					{
						pumpA.sendDeactivate();
						pumpB.sendDeactivate();
						gate.sendOpenGate();
						signals.switchLampBOn();
						state = FSMState.OpeningGate;
					}
					break;
				
				case OpeningGate: 
					
					if(gate.receivedGateOpen())
					{
						signals.switchLampBOff();
						state = FSMState.CheckingHumidity;
					}
					break;
					
			}
		//}	
	}

}
